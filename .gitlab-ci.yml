variables:
  REPORT_FILENAME: gl-sast-report.json
  MAX_IMAGE_SIZE_MB: 200
  MAX_IMAGE_SIZE_MB_FIPS: 1090
  SAST_EXCLUDED_ANALYZERS: "bandit,eslint,semgrep"
  SEARCH_MAX_DEPTH: 20
  
include:
  - remote: https://gitlab.com/gitlab-org/security-products/ci-templates/raw/master/includes-dev/analyzer.yml

  # Include Reviewers.gitlab-ci.yml to recommend code reviewers for a given MR
  - project: gitlab-org/modelops/applied-ml/review-recommender/ci-templates
    ref: v0.2.0
    file: recommender/Reviewers.gitlab-ci.yml

reviewers-recommender:
  stage: pre-build
  rules:
    - if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH

semgrep-rules-test:
  stage: test
  image: $CI_REGISTRY_IMAGE/tmp:$CI_COMMIT_SHA # TMP_IMAGE
  script: cd semgrep_rules_check && ./run_check.sh
  artifacts:
    paths:
      - ./semgrep_rules_check/received.json

semgrep-meta-rules:
  stage: test
  image: returntocorp/semgrep-agent:v1
  script: semgrep-agent --config p/semgrep-rule-lints
  allow_failure: true

.qa-downstream-sast:
  variables:
    DS_DEFAULT_ANALYZERS: ""
    SAST_EXCLUDED_ANALYZERS: "bandit,eslint"
    SAST_EXCLUDED_PATHS: "" # TEMP: until https://gitlab.com/gitlab-org/gitlab/-/issues/223283

python-pip-flask-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pip-flask/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/python-pip-flask

python-pip-flask-custom-rulesets-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pip-flask-custom-rulesets/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/python-pip-flask
    branch: custom-rulesets-FREEZE

python-pip-flask-custom-rulesets-with-passthrough-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pip-flask-custom-rulesets-with-passthrough/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/python-pip-flask
    branch: custom-rulesets-with-passthrough-FREEZE

python-pip-flask-custom-rulesets-with-raw-passthrough-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pip-flask-custom-rulesets-with-passthrough/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/python-pip-flask
    branch: custom-rulesets-with-raw-passthrough-FREEZE

python-pip-multi-project-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pip-multi-project/gl-sast-report.json"
    SAST_EXCLUDED_ANALYZERS: "eslint"
  trigger:
    project: gitlab-org/security-products/tests/python-pip
    branch: multi-project-FREEZE

python-pip-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pip/gl-sast-report.json"
    SAST_EXCLUDED_ANALYZERS: "eslint"
  trigger:
    project: gitlab-org/security-products/tests/python-pip

python-pipenv-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/python-pipenv/gl-sast-report.json"
    SAST_EXCLUDED_ANALYZERS: "eslint"
  trigger:
    project: gitlab-org/security-products/tests/python-pipenv

c-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/c/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/c

c-fips-qa:
  extends: .qa-downstream-sast-fips
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/c/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/c

js-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/js/gl-sast-report.json"
    SAST_EXCLUDED_ANALYZERS: "nodejs-scan"
  trigger:
    project: gitlab-org/security-products/tests/js
    branch: jsx-FREEZE

go-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/go/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/go

go-custom-ruleset-synthesis-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/go-custom-ruleset-synthesis/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/go
    branch: Custom-Ruleset-Synthesis-FREEZE

java-maven-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/java-maven

java-maven-multimodules-qa:
  extends: .qa-downstream-sast
  variables:
    SAST_REPORT_URL: "$CI_PROJECT_URL/raw/$CI_COMMIT_REF_NAME/qa/expect/java-maven-multimodules/gl-sast-report.json"
  trigger:
    project: gitlab-org/security-products/tests/java-maven-multimodules
    branch: semgrep-migration-FREEZE

